package basics.arrays;

import java.util.*;

public class ArrayMultiDimentional {

	public static void main(String[] args) {
		@SuppressWarnings("resource")
		Scanner in = new Scanner(System.in);

		System.out.print("Enter the number of rows ? ");
		int rows = in.nextInt();

		System.out.print("Enter the numbers of columns ? ");
		int cols = in.nextInt();

		int[][] table = new int[rows][cols];
		populateArray(table);
		printArray(table);
	}

	private static void populateArray(int x[][]) {
		for (int i = 0; i < x.length; i++) {

			for (int j = 0; j < x[i].length; j++) {
				x[i][j] = (int) (Math.random() * (j + 5));
			}
		}
	}

	private static void printArray(int x[][]) {
		for (int i = 0; i < x.length; i++) {

			for (int j = 0; j < x[i].length; j++) {
				System.out.printf(x[i][j] + " ");
			}

			System.out.printf("\n");

		}
	}
}
