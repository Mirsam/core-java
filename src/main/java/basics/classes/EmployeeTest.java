package basics.classes;

import java.time.LocalDate;

public class EmployeeTest {

	public static void main(String[] args) {
		Employee[] staff = new Employee[3];
		
		staff[0] = new Employee("Rukaya", 20000, 1999, 2, 12);
		staff[1] = new Employee("Umar", 22000, 1999, 4, 22);
		staff[2] = new Employee("Amir", 25000, 1999, 12, 2);
		
		for (Employee e : staff) {
			e.raiseSalary(5);
		}
		
		for(Employee e : staff) {
			System.out.println("Name  " + e.getName() + "   Salary  " + e.getSalary() + " HireDay  " + e.getHireDay());
		}
	}

}

class Employee {
	private String name;
	private double salary;
	private LocalDate hireDay;
	
	public Employee(String n, double s, int year, int month, int day) {
		name = n;
		salary = s;
		hireDay = LocalDate.of(year, month, day);
	}
	
	public String getName() {
		return name;
	}
	
	public double getSalary() {
		return salary;
	}
	
	public LocalDate getHireDay() {
		return hireDay;
	}
	
	public void raiseSalary(double byPercent) {
		double raise = salary * byPercent / 100;
		salary += raise;
	}
}