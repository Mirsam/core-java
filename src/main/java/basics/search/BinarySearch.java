package basics.search;

import java.util.Scanner;

class BinarySearch {
	public static void main(String args[]) {

		int c, first, last, middle, no_Of_Elements, key, arr[];

		Scanner in = new Scanner(System.in);
		System.out.print("Enter number of elements ? ");
		no_Of_Elements = in.nextInt();
		arr = new int[no_Of_Elements];

		System.out.print("Enter " + no_Of_Elements + " Integers separated by space ?");

		for (int i = 0; i < no_Of_Elements; i++) {
			arr[i] = in.nextInt();
		}

		System.out.print("Enter the value to be found ? ");

		key = in.nextInt();

		first = 0;
		last = no_Of_Elements - 1;
		middle = (first + last) / 2;

		while (first <= last) {
			if (arr[middle] < key)
				first = middle + 1;
			else if (arr[middle] == key) {
				System.out.println(key + " found at location " + (middle + 1) + ".");
				break;
			} else
				last = middle + 1;
			middle = (first + last) / 2;
		}
		if (first > last)
			System.out.println(key + " is not present in the list.\n");
	}
}
